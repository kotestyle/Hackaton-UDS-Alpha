import numpy as np

from config import Config as config
from text_features import TextFeatures, create_nn_features
from preprocessing import DataPreparation, filter_df, get_cummulative
from train_model import train_lgb_regressor
from train_model import train_catboost_regressor


def main():
    dp = DataPreparation()
    dp.prepare_train_data()
    dp.add_cat_and_geo_features()
    dp.add_CPV_features()
    dp.add_numerical_features()
    dp.add_date_features()
    dp.add_complaints_features()
    dp.add_org_features()

    data, test = dp.get_train_test_data()
    data = filter_df(data)

    tf = TextFeatures(data, test)
    tf.create_text_features()
    # tf.get_text_features()
    create_nn_features(tf.train_df, tf.test_df)
    del tf

    train = data[data.train_val == 'train']
    val = data[data.train_val == 'val']

    for feat in config.cummulative:
        train, val, df_test = get_cummulative(train, val, test, feat, df_calc=train)

    x_train = train[config.features]
    x_test = val[config.features]
    y_train = np.log1p(train['counts'])
    y_test = np.log1p(val['counts'])

    lgb_model = train_lgb_regressor(x_train, x_test, y_train, y_test)
    # cb_model = train_catboost_regressor(x_train, x_test, y_train, y_test)
    #
    # cb_preds = cb_model.predict(test[config.features])
    # test['target'] = np.exp(cb_preds) - 1
    # test[['unique_id', 'target']].to_csv('cb.csv', index=False)
    #
    lgb_preds = lgb_model.predict(test[config.features])
    test['target'] = np.exp(lgb_preds) - 1
    test[['target']].to_csv('lgb.csv', index=True)


if __name__ == '__main__':
    main()
