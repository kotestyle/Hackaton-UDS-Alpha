import functools
import pandas as pd
import numpy as np
import scipy.stats as spstats

from collections import defaultdict
from sklearn.preprocessing import LabelEncoder

from config import Config as config

########################################################################################################################
########################################################################################################################


def filter_df(df):
    df = df[df['LotStatus'] == 'Завершено лот закупівлі']
    df = df[df['TenderStatus'] == 'complete']
    return df


def get_cummulative(df1, df2, df3, feature, df_calc):
    x = df_calc[[feature, 'StartDateCorrection', 'counts']].groupby(
        [feature, 'StartDateCorrection']).sum().shift().groupby(level=[0]).cumsum().reset_index()
    y = df_calc[[feature, 'StartDateCorrection', 'counts']].groupby([feature, 'StartDateCorrection']).sum().groupby(
        level=[0]).cumcount().reset_index()
    x = pd.merge(x, y)

    x['mean'] = x.counts / (x[0])
    x.replace([np.inf], np.nan, inplace=True)  # .
    x.dropna(subset=['mean'], inplace=True)
    x.drop([0], axis=1, inplace=True)
    x.columns.values[2] = feature + '_total'
    x.columns.values[3] = feature + '_mean'

    y = df_calc[[feature, 'counts']].groupby(feature).agg({'counts': [sum, np.mean]})
    y.columns = y.columns.droplevel()
    y = y.reset_index()
    y.columns.values[1] = feature + '_total'
    y.columns.values[2] = feature + '_mean'

    return pd.merge(df1, x, 'left'), pd.merge(df2, y, 'left'), pd.merge(df3, y, 'left')

########################################################################################################################
########################################################################################################################


class DataPreparation:
    def __init__(self):
        self.train_df = pd.read_csv(config.PATH_TO_TRAIN_DATA)
        self.test_df = pd.read_csv(config.PATH_TO_TEST_DATA)

        self.__rename_columns()
        self.__convert_datetime()
        self.__add_val_column()

    def __rename_columns(self):
        self.train_df.columns = config.eng_columns_train
        self.test_df.columns = config.eng_columns_test

    def __convert_datetime(self):
        for df in [self.train_df, self.test_df]:
            df['StartOfferDate'] = pd.to_datetime(df['StartOfferDate'])
            df['StartDateCorrection'] = pd.to_datetime(df['StartDateCorrection'],
                                                       errors='coerce')
            df['EndDate'] = pd.to_datetime(df['EndDate'])
            df['EndOfferDate'] = pd.to_datetime(df['EndOfferDate'])

    def __add_val_column(self):
        self.test_df['train_val'] = 'test'

        self.train_df['train_val'] = np.where(
            (self.train_df.EndDate > "2017-06-30")
            & (self.train_df.LotStatus == 'Завершено лот закупівлі')
            & (self.train_df.TenderStatus == 'complete'),
            'val',
            'train'
        )

    def prepare_train_data(self):

        self.train_df.set_index('unique_id', inplace=True)
        self.test_df.set_index('unique_id', inplace=True)

        self.train_df = self.train_df[[c for c in self.train_df.columns if c in self.test_df.columns]]
        _ = self.train_df.groupby(self.train_df.index).head(1).sort_index()
        _['counts'] = self.train_df.groupby(self.train_df.index).size().sort_index()
        self.train_df = _

    def __encode_categorical(self, cat_cols=config.cat_cols):
        # encode categorical data
        d = defaultdict(LabelEncoder)

        # fit and encode train/test
        a = pd.concat([df[cat_cols] for df in [self.train_df, self.test_df]], axis=0).fillna('').apply(
            lambda x: d[x.name].fit(x))
        # transform encodings to train/test
        for df in [self.train_df, self.test_df]:
            df[cat_cols] = df[cat_cols].fillna('').apply(lambda x: d[x.name].transform(x))

    def __create_geo_features(self, geo_mapping_df=config.geo_mapping_coord):
        lst = []
        for df in [self.train_df, self.test_df]:
            df_geo = df.reset_index()[[
                'unique_id',
                'DeliveryRegion',
                'OrgRegion'
            ]].copy(deep=True)

            # 1. DeliveryRegion
            # clean some garbage (heuristics)

            # strip and lowercase, take 6 first chars
            df_geo.loc[df_geo.DeliveryRegion == '-', 'DeliveryRegion'] = 'others'
            df_geo.DeliveryRegion = df_geo.DeliveryRegion.str.strip() \
                                        .str.lower().str[:6]

            df_geo.loc[df_geo.DeliveryRegion == 'одесск',
                       'DeliveryRegion'] = 'одеськ'
            df_geo.loc[df_geo.DeliveryRegion == 'украин',
                       'DeliveryRegion'] = 'україн'

            df_geo.loc[df_geo.DeliveryRegion == 'харько',
                       'DeliveryRegion'] = 'харків'

            true_regions = set(geo_mapping_df.index.values.tolist())

            # fill-in all other
            df_geo.loc[
                ~df_geo.DeliveryRegion.isin(true_regions),
                'DeliveryRegion'
            ] = 'others'

            # estimate garbage/missing percent
            print('garbage/missing %: {:.2f}'.format(
                df_geo[df_geo.DeliveryRegion == 'others'].shape[0] /
                df_geo.shape[0]
            ))

            # 2. OrgRegions
            # replace missing and clean data
            df_geo.loc[df_geo.OrgRegion == '-', 'OrgRegion'] = 'others'
            df_geo.OrgRegion = df_geo.OrgRegion.str[:6].str.lower()

            # add lat/long

            df_geo['OrgRegion_lat'] = df_geo.OrgRegion.map(geo_mapping_df['lat']).fillna(0)
            df_geo['OrgRegion_long'] = df_geo.OrgRegion.map(geo_mapping_df['long']).fillna(0)

            df_geo['DeliveryRegion_lat'] = df_geo.DeliveryRegion.map(
                geo_mapping_df['lat']).fillna(0)
            df_geo['DeliveryRegion_long'] = df_geo.DeliveryRegion.map(
                geo_mapping_df['long']).fillna(0)

            # cnange strings to label encoded
            df_geo.OrgRegion = df_geo.OrgRegion.map(
                geo_mapping_df['LabelEncoderID']).fillna(-1)
            df_geo.DeliveryRegion = df_geo.DeliveryRegion.map(
                geo_mapping_df['LabelEncoderID']).fillna(-1)

            # check is equal
            df_geo['DeliveryOrg_isequal'] = (df_geo.OrgRegion == df_geo.DeliveryRegion)

            # return distance for non-equal
            df_geo['DeliveryOrg_distance'] = -1
            df_geo.loc[~df_geo.DeliveryOrg_isequal, 'DeliveryOrg_distance'] = \
                df_geo.loc[~df_geo.DeliveryOrg_isequal,
                ].apply(
                    lambda x: np.linalg.norm(
                        np.array([x['DeliveryRegion_lat'], x['DeliveryRegion_long']])
                        - np.array([x['OrgRegion_lat'], x['OrgRegion_long']]))
                    , axis=1
                )

            # lst.append(df.drop(['DeliveryRegion', 'OrgRegion'], axis=1, errors='ignore').merge(
            #     right=df_geo.set_index('unique_id'),
            #     left_index=True,
            #     right_index=True
            # ))
            lst.append(df.drop(['DeliveryRegion', 'OrgRegion'], axis=1, errors='ignore').merge(
                right=df_geo.set_index('unique_id'),
                left_index=True,
                right_index=True
            ))

        self.train_df, self.test_df = lst[0], lst[1]

    def add_cat_and_geo_features(self):

        # perform categorical encoding inplace
        self.__encode_categorical()

        # process geo-features and concat it with the initial df for each df in list
        self.__create_geo_features()

    @staticmethod
    def __CPV_features(df, split_type=2):
        def strip(x, n):
            return x.strip()[:n].strip()

        df['CPV2017'] = df['CPV2017'].apply(lambda x: '00000000-0' if strip(x, 10) == '-' else x)

        if split_type == 1:
            df['CPV_unit'] = df['CPV2017'].apply(lambda x: int(x.strip()[:2]))
            df['CPV_group'] = df['CPV2017'].apply(lambda x: int(x.strip()[2:3]))
            df['CPV_class'] = df['CPV2017'].apply(lambda x: int(x.strip()[3:4]))
            df['CPV_cat'] = df['CPV2017'].apply(lambda x: int(x.strip()[4:5]))
        else:
            df['CPV_unit'] = df['CPV2017'].apply(lambda x: int(x.strip()[:2]))
            df['CPV_group'] = df['CPV2017'].apply(lambda x: int(x.strip()[:3]))
            df['CPV_class'] = df['CPV2017'].apply(lambda x: int(x.strip()[:4]))
            df['CPV_cat'] = df['CPV2017'].apply(lambda x: int(x.strip()[:5]))

        df['CPV_desc'] = df['CPV2017'].apply(lambda x: x[11:])
        df['CPV_desc'] = df['CPV_desc'].apply(
            lambda x: "NO DESC" if len(x.strip()) < 2 else x)

        df['Organizer'] = df['Organizer'].apply(lambda x: x.lower())
        df['Organizer_railroad'] = df['Organizer'].apply(
            lambda x: 1 if 'залізниц' in x else 0)
        df['Organizer_ministry'] = df['Organizer'].apply(
            lambda x: 1 if 'міністерств' in x else 0)
        df['Organizer_komun'] = df['Organizer'].apply(
            lambda x: 1 if 'комунальне підприємство'
                           in x else 0)
        df['Organizer_edu'] = df['Organizer'].apply(
            lambda x: 1 if 'освіт' in x else 0)

        return df

    def add_CPV_features(self):
        self.train_df = self.__CPV_features(self.train_df)
        self.test_df = self.__CPV_features(self.test_df)

    def __boxcox_feature(self, feature_name, df):

        feature_np = np.array(df[feature_name])
        feature_np_clean = feature_np[~np.isnan(feature_np)]
        l, opt_lambda = spstats.boxcox(feature_np_clean)
        df[feature_name + '_0'] = spstats.boxcox((1 + df[feature_name]), lmbda=0)
        df[feature_name + '_0'] = df[feature_name + '_0'].fillna(0.0)
        df[feature_name + '_boxcox_lambda_opt'] = spstats.boxcox(df[feature_name], lmbda=opt_lambda)
        df[feature_name + '_boxcox_lambda_opt'] = df[feature_name + '_boxcox_lambda_opt'].fillna(0.0)

        return df

    def __transfrom_numerical(self, df, features_numerical):
        for feature in features_numerical:
            df[feature] = df[feature].apply(pd.to_numeric, errors='coerce')
        df[features_numerical] = df[features_numerical].fillna(0)
        for feature in features_numerical:
            df[feature + '_isZero'] = df[feature].apply(lambda x: 1 if x == 0.0 else 0)
        df[features_numerical] = df[features_numerical].replace(0.0, np.nan)
        for feature in features_numerical:
            df = self.__boxcox_feature(feature, df)
        return df

    def add_numerical_features(self):
        self.train_df = self.__transfrom_numerical(self.train_df, features_numerical=config.features_numerical)
        self.test_df = self.__transfrom_numerical(self.test_df, features_numerical=config.features_numerical)

    @staticmethod
    def __date_features(df, date_columns):
        for c in date_columns:
            df[c + '_year'] = df[c].dt.year
            df[c + '_month'] = df[c].dt.month
            df[c + '_dow'] = df[c].dt.dayofweek
        return df

    @staticmethod
    def get_date_diff(df, date1, date2):
        df[date1 + '_diff_' + date2] = (df[date1] - df[date2]) / np.timedelta64(1, 'D')
        return df

    def add_date_features(self):
        for df in [self.train_df, self.test_df]:
            df = self.__date_features(df, ['StartOfferDate'])

        for df in [self.train_df, self.test_df]:
            df = self.get_date_diff(df, 'EndOfferDate', 'StartOfferDate')
            df = self.get_date_diff(df, 'EndDate', 'StartDateCorrection')
            df = self.get_date_diff(df, 'EndDate', 'EndOfferDate')
            df = self.get_date_diff(df, 'StartOfferDate', 'StartDateCorrection')

    def add_complaints_features(self):
        df_complaints = pd.read_csv(config.PATH_TO_COMPLAINTS_DATA, engine='c')
        df_complaints.columns = config.complaints_columns

        # drop "garbage"
        df_complaints = df_complaints[~df_complaints.complain_date.str.contains('-')]

        df_complaints.complain_date = pd.to_datetime(
            df_complaints.complain_date, format='%d.%m.%Y'
        )
        # filter, non-future
        df_complaints = df_complaints[df_complaints.complain_date < "2017-06-30"]

        df_complaints['IDOrganizator'] = df_complaints.unique_id.map(
            self.train_df.IDOrganizator
        ).astype(np.int32)

        compl_by_org = df_complaints.groupby('IDOrganizator').agg(
            {
                'unique_id': lambda x: len(np.unique(x)),
            })

        compl_by_org['lots_in_train'] = self.train_df[self.train_df.train_val == 'train'].reset_index().groupby(
            'IDOrganizator').agg(
            {
                'unique_id': lambda x: len(np.unique(x)),
            }).unique_id


        compl_by_org['lot_complain_ratio'] = (
                compl_by_org['unique_id'] / compl_by_org['lots_in_train']
        ).fillna(0).astype(np.float32)

        compl_by_org.loc[compl_by_org.lot_complain_ratio > 1, 'lot_complain_ratio'] = 1

        self.train_df['lot_complain_ratio'] = self.train_df.IDOrganizator.map(
            compl_by_org.lot_complain_ratio).fillna(0)
        self.test_df['lot_complain_ratio'] = self.test_df.IDOrganizator.map(
            compl_by_org.lot_complain_ratio).fillna(0)

    @staticmethod
    def __get_organizator_lots_count(id_organizator, organizator_lots_count):
        if id_organizator in organizator_lots_count:
            return np.log(organizator_lots_count[id_organizator])
        else:
            return 0

    def add_org_features(self):

        _ = pd.concat([self.train_df[['IDOrganizator']], self.test_df[['IDOrganizator']]], axis=0)
        _ = dict(_['IDOrganizator'].value_counts())

        for df in [self.train_df, self.test_df]:
            df['organizator_lots_count'] = df['IDOrganizator'].map(
                functools.partial(self.__get_organizator_lots_count, organizator_lots_count=_))

    def get_train_test_data(self):
        return self.train_df, self.test_df

