import numpy as np
import pandas as pd
import lightgbm as lgb
from catboost import CatBoostRegressor

from config import Config as config


def rmsle(y_true, y_pred):
    return np.square(np.log(y_pred + 1) - np.log(y_true + 1)).mean() ** 0.5


def rmsle_lgb(preds, df):
    labels = df.get_label()
    return 'rmsle', rmsle(labels, preds), False


def train_lgb_regressor(x_train, x_test, y_train, y_test):
    lgb_train = lgb.Dataset(x_train, label=y_train, free_raw_data=False)
    lgb_val = lgb.Dataset(x_test, label=y_test, free_raw_data=False, reference=lgb_train)

    model = lgb.train(
        config.lgbm_params,
        lgb_train,
        1000,
        valid_sets=[lgb_train, lgb_val],
        early_stopping_rounds=40
    )

    return model


def train_catboost_regressor(x_train, x_test, y_train, y_test, all=True):
    model = CatBoostRegressor(loss_function='RMSE', eval_metric='RMSE', thread_count=30,
                              random_seed=42, iterations=750,
                              learning_rate=0.075, depth=7, l2_leaf_reg=2.5, rsm=1.0)
    if all:
        x = pd.concat([x_train, x_test], axis=0)
        y = pd.concat([y_train, y_test], axis=0)
        model.fit(x, y)
    else:
        model.fit(x_train, y_train, eval_set=(x_test, y_test), use_best_model=True, verbose=True)

    return model
