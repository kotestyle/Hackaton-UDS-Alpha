import tokenize_uk
import pandas as pd
import numpy as np
import keras as ks
import tensorflow as tf

from config import Config as config
from functools import partial
from operator import itemgetter
from multiprocessing.pool import ThreadPool

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Ridge
from sklearn.model_selection import cross_val_predict, KFold
from sklearn.metrics import make_scorer, mean_squared_log_error
from sklearn.pipeline import make_pipeline, make_union, Pipeline
from sklearn.preprocessing import FunctionTransformer, StandardScaler


########################################################################################################################
########################################################################################################################

def preprocess(text):
    return ' '.join(tokenize_uk.tokenize_words(text)).lower()


def rmsle(y_true, y_pred):
    assert len(y_true) == len(y_pred)
    return np.square(np.log(y_pred + 1) - np.log(y_true + 1)).mean() ** 0.5


def on_field(f: str, *vec) -> Pipeline:
    return make_pipeline(FunctionTransformer(itemgetter(f), validate=False), *vec)


def fit_predict(xs, y_train) -> np.ndarray:
    X_train, X_test = xs
    config = tf.ConfigProto(
        intra_op_parallelism_threads=1, use_per_session_threads=1, inter_op_parallelism_threads=1)
    with tf.Session(graph=tf.Graph(), config=config) as sess:
        ks.backend.set_session(sess)
        model_in = ks.Input(shape=(X_train.shape[1],), dtype='float32', sparse=True)
        out = ks.layers.Dense(192, activation='relu')(model_in)
        out = ks.layers.Dense(64, activation='relu')(out)
        out = ks.layers.Dense(64, activation='relu')(out)
        out = ks.layers.Dense(1)(out)
        model = ks.models.Model(model_in, out)
        model.compile(loss='mean_squared_error', optimizer=ks.optimizers.Adam(lr=3e-3))
        for i in range(3):
            model.fit(x=X_train, y=y_train, batch_size=2 ** (11 + i), epochs=1, verbose=2)
        return model.predict(X_test)[:, 0]


def create_nn_features(data, test):
    vectorizer = make_union(
        on_field('Tender_text_prep', TfidfVectorizer(max_features=5000, min_df=10, token_pattern='\w+')),
        on_field('LotDescr_text_prep', TfidfVectorizer(max_features=5000, min_df=10, token_pattern='\w+')),
        on_field('Organizer_text_prep', TfidfVectorizer(max_features=5000, min_df=10, token_pattern='\w+')),
        on_field('CPV_desc_text_prep', TfidfVectorizer(max_features=5000, min_df=10, token_pattern='\w+')),
        n_jobs=4)
    y_scaler = StandardScaler()

    prediction_nn = pd.Series(index=data.index)

    cv = KFold(n_splits=3, shuffle=True, random_state=42)
    for train_ids, valid_ids in cv.split(data):
        train, valid = data.iloc[train_ids], data.iloc[valid_ids]
        y_train = y_scaler.fit_transform(np.log1p(train['counts'].values.reshape(-1, 1)))
        X_train = vectorizer.fit_transform(train).astype(np.float32)
        X_valid = vectorizer.transform(valid).astype(np.float32)
        with ThreadPool(processes=4) as pool:
            Xb_train, Xb_valid = [x.astype(np.bool).astype(np.float32) for x in [X_train, X_valid]]
            xs = [[Xb_train, Xb_valid], [X_train, X_valid]] * 2
            y_pred = np.mean(pool.map(partial(fit_predict, y_train=y_train), xs), axis=0)
        y_pred = np.expm1(y_scaler.inverse_transform(y_pred.reshape(-1, 1))[:, 0])
        prediction_nn.iloc[valid_ids] = y_pred
        print('Valid RMSLE: {:.4f}'.format(np.sqrt(mean_squared_log_error(valid['counts'], y_pred))))
    data['prediction_text_nn'] = prediction_nn.values

    y_train = y_scaler.fit_transform(np.log1p(data['counts'].values.reshape(-1, 1)))
    X_train = vectorizer.fit_transform(data).astype(np.float32)
    X_valid = vectorizer.transform(test).astype(np.float32)
    # Xb_train, Xb_valid = [x.astype(np.bool).astype(np.float32) for x in [X_train, X_valid]]
    with ThreadPool(processes=4) as pool:
        Xb_train, Xb_valid = [x.astype(np.bool).astype(np.float32) for x in [X_train, X_valid]]
        xs = [[Xb_train, Xb_valid], [X_train, X_valid]] * 2
        y_pred = np.mean(pool.map(partial(fit_predict, y_train=y_train), xs), axis=0)
    y_pred = np.expm1(y_scaler.inverse_transform(y_pred.reshape(-1, 1))[:, 0])
    test['prediction_text_nn'] = y_pred

    data = data[['prediction_text_nn']]
    test = test[['prediction_text_nn']]

    return data, test


########################################################################################################################
########################################################################################################################

class TextFeatures:
    def __init__(self, train_df, test_df):
        self.train_df = train_df
        self.test_df = test_df

        self.rmsle_scorer = make_scorer(rmsle)

        self.model = Pipeline([
            ("Tfidf_vectorizer", TfidfVectorizer(ngram_range=(1, 1), min_df=10, max_features=5000)),
            ("lr", Ridge(solver='auto', fit_intercept=True, alpha=0.4, max_iter=250, normalize=False, tol=0.01))
        ])

        self.train_text_features = None
        self.test_text_features = None

        self.__add_cpv_desc()
        self.__preprocess()

    def __add_cpv_desc(self):
        for df in [self.train_df, self.test_df]:
            df['CPV_desc'] = df['CPV2017'].apply(lambda x: x[11:])
            df['CPV_desc'] = df['CPV_desc'].apply(lambda x: "NO DESC" if len(x.strip()) < 2 else x)

    def __preprocess(self):
        for feature in config.text_features:
            _ = feature + '_text_prep'

            self.train_df[_] = self.train_df[feature].map(preprocess)
            self.test_df[_] = self.test_df[feature].map(preprocess)

    def create_text_features(self):

        for feature in config.text_features:
            _ = feature + '_text_prep'

            y_pred = cross_val_predict(self.model, self.train_df[_], self.train_df['counts'])
            self.train_df['prediction_' + feature] = y_pred

            self.model.fit(self.train_df[_], self.train_df['counts'])
            y_pred = self.model.predict(self.test_df[_])
            self.test_df['prediction_' + feature] = y_pred

        self.train_text_features = self.train_df[config.text_features_predicted]
        self.test_text_features = self.test_df[config.text_features_predicted]

    def get_text_features(self):
        return self.train_text_features, self.test_text_features
