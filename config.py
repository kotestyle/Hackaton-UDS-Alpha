import os
import pandas as pd


class Config:
    APP_DIR = os.path.dirname(os.path.abspath(__file__))

    DATA_DIR = os.path.join(APP_DIR, 'data')

    eng_columns_train = [
        'CPV2017', 'SumTender', 'Participant', 'Organizer', 'ID', 'IDLOT', '1RoundSum', '2RoundSum',
        '3RoundSum', 'StartOfferDate', 'TypeMethod', 'TypeOrginizer', 'LotStatus', 'TenderStatus',
        'OrgRegion', 'ParticipantRegion', 'DeliveryRegion', 'result', 'Tender', 'StartDateCorrection',
        'LotDescr', 'IDOrganizator', 'OfferDate', 'EndDate', 'EndOfferDate', 'StepDecreaseLot',
        'GuaranteeValueLot', 'unique_id'
    ]

    eng_columns_test = [
        'CPV2017', 'SumTender', 'Organizer', 'ID', 'IDLOT', 'StartOfferDate', 'TypeMethod', 'TypeOrginizer',
        'LotStatus', 'TenderStatus', 'OrgRegion', 'DeliveryRegion', 'Tender', 'StartDateCorrection', 'LotDescr',
        'IDOrganizator', 'EndDate', 'EndOfferDate', 'StepDecreaseLot', 'GuaranteeValueLot', 'unique_id'
    ]

    complaints_columns = [
        'complainer',
        'Organizator',
        'complain_city',
        'ID',
        'IDLOT',
        'complain_tender',
        'complain_status',
        'complain_type',
        'complain_file_date',
        'complain_description',
        'complain_date',
        'unique_id',
    ]

    PATH_TO_TRAIN_DATA = os.path.join(DATA_DIR, 'train_data.csv')
    PATH_TO_TEST_DATA = os.path.join(DATA_DIR, 'test_data.csv')
    PATH_TO_COMPLAINTS_DATA = os.path.join(DATA_DIR, 'complaints_train.csv')

    text_features = [
        'Tender',
        'LotDescr',
        'Organizer',
        'CPV_desc'
    ]

    text_features_predicted = [
        'prediction_Tender',
        'prediction_LotDescr',
        'prediction_Organizer',
        'prediction_CPV_desc'
    ]

    geo_mapping_coord = pd.DataFrame(
        {
            # '6 chars lowercased': (LabelEncoder ID, lat, long)
            'київсь': (1, 50.4021368, 30.2525113),
            'дніпро': (2, 48.4624412, 34.8602734),
            'донець': (3, 47.9902621, 37.6214375),
            'вінниц': (4, 49.2348249, 28.3995942),
            'запорі': (5, 47.8563742, 35.0352705),
            'львівс': (6, 49.8327787, 23.9421959),
            'черніг': (7, 51.495866, 31.2204989),
            'одеськ': (8, 46.460123, 30.5717042),
            'харків': (9, 49.9947277, 36.1457419),
            'житоми': (10, 50.2679751, 28.6036779),
            'микола': (11, 46.9332135, 31.8679138),
            'сумськ': (12, 50.9007528, 34.7441743),
            'волинс': (13, 50.7398786, 25.2639652),
            'полтав': (14, 49.6021346, 34.4871989),
            'хмельн': (15, 49.4106425, 26.9252189),
            'івано-': (16, 48.9118242, 24.6821096),
            'черкас': (17, 49.4312235, 31.9791903),
            'рівнен': (18, 50.6111564, 26.1745436),
            'луганс': (19, 48.5802062, 39.2168845),
            'терноп': (20, 49.5484448, 25.5276293),
            'херсон': (21, 46.6496689, 32.5377419),
            'кірово': (22, 48.518858, 32.1456233),
            'чернів': (23, 51.495866, 31.2204989),
            'закарп': (24, 48.496582, 22.8212266),
            'украин': (25, 48.2559613, 26.6958946),  # украина
            'відпов': (26, 0, 0),  # відповідно документу
            'others': (-1, 0, 0),  # missing / other
        }).T

    geo_mapping_coord.columns = ['LabelEncoderID', 'lat', 'long']

    cat_cols = ['TypeOrginizer', 'TypeMethod']

    features_numerical = ['GuaranteeValueLot', 'StepDecreaseLot', 'SumTender']
    cummulative = ['CPV_unit', 'IDOrganizator', 'OrgRegion', 'DeliveryRegion']

    lgbm_params = {

        'application': 'poisson',
        'num_leaves': 127,  # was 127
        'learning_rate': 0.15,  # was 0.02
        'max_depth': 11,  # was 7

        'min_child_samples': 60,
        'metric': 'rmse',

        'feature_fraction': 0.8,  # was 0.7
        'sub_row': 0.75,  # was 0.7
        'num_threads': -1,
        'verbose': 0,
        'seed': 42,

    }

    features = []

    # features += ['TenderStatus', 'LotStatus']

    features += ['TypeMethod', 'TypeOrginizer']

    features += ['OrgRegion', 'DeliveryRegion']
    features += ['DeliveryOrg_isequal']
    features += ['DeliveryOrg_distance']

    features += ['GuaranteeValueLot_isZero', 'StepDecreaseLot_isZero',
                 'SumTender_isZero']
    features += ['GuaranteeValueLot_0', 'StepDecreaseLot_0', 'SumTender_0']
    # features = ['GuaranteeValueLot_boxcox_lambda_opt', 'StepDecreaseLot_boxcox_lambda_opt',
    #             'SumTender_boxcox_lambda_opt']

    features += ['Organizer_railroad', 'Organizer_ministry', 'Organizer_komun', 'Organizer_edu']
    features += ['CPV_unit', 'CPV_group', 'CPV_class', 'CPV_cat']

    features += ['prediction_Tender', 'prediction_LotDescr', 'prediction_Organizer', 'prediction_CPV_desc']
    features += ['prediction_text_nn']

    # features += ['organizator_lots_count']
    # features += ['org_complaints_ratio']
    features += ['lot_complain_ratio']
    # features += ['organizator_props_count', 'organizator_participant_count']

    features += ['StartOfferDate_year',
                 'StartOfferDate_month', 'StartOfferDate_dow', ]
    features += ['EndOfferDate_diff_StartOfferDate', 'EndDate_diff_StartDateCorrection',
                 'EndDate_diff_EndOfferDate', 'StartOfferDate_diff_StartDateCorrection']
